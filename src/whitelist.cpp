#include "whitelist.h"

using namespace std;

void whitelist::add_to_whitelist(const string &student_name, const student &_student)
{
  if (whitelist::is_student_present(student_name))
  {
    *student_record.find(student_name)->second = _student;
  }
  else
  {
    student_data_list.push_back(_student);
    student_record.insert({student_name, &student_data_list.back()});
  }
}
const bool whitelist::is_student_present(const string &student_name) const
{
  auto search = student_record.find(student_name);
  if (search != student_record.end())
  {
    return true;
  }
  return false;
}
const student *whitelist::get_student_data(const string &student_name) const
{
  auto search = student_record.find(student_name);
  if (search != student_record.end())
  {
    return search->second;
  }
  return nullptr;
}
