# Unit Testing

This repo is for practicing some unit testing concepts.

### How to configure, build and run

**Configure** :
- Clone this repo and ``cd`` to the newly created local dir.
- Run ``mkdir build`` to create a build dir for configuration and build files. 
- Navigate to ``<project_root_dir>/build``.
- Run ``cmake ..`` to congigure in ``Release`` or ``cmake -DCMAKE_BUILD_TYPE=Debug ..`` to congigure in ``Debug`` and ``Release`` mode.

**Build** : 
- Navigate to ``<project_root_dir>/build``.
- Run ``make`` build the executable or run ``cmake --build .``.

**Execute / Run** : 
- Navigate to ``<project_root_dir>/build``.
- Run ``./app/student_entry`` or ``./app/student_entry_debug`` to execute in release or debug mode respectively.

### ``assert`` and ``static_assert`` functions 

These are preprocessor macros that are used to test assertions.<br/>
- ``assert`` tests assertions on run time<br/>
- ``static_assert`` tests assertions on compile time<br/>

### Add ``googletest`` test as submodule 

- Used ``git submodule add <https_clone_link>`` to clone the gtest repo in main dir of project.
- Added ``add_subdirectory(googletest)`` in root ``CMakeLists.txt`` file.

### ``test`` dir to include unit tests 

- In ``CMakeLists.txt`` file link ``gtest_main`` and ``student_entry_lib`` libraries with ``target_link_libraries``.
- In ``test.cpp`` write some tests and run them in main.

### Write tests 

- Tests are written to test functionality of class functions like data entry and return values.


