#include "whitelist.h"
#include <cassert>

using namespace std;

//  To initialize student data in whitelist
inline void initailize(whitelist *_whitelist);
int array_size(string name);

// constant expression, defined at compile time, used for array size and in assertion macro
constexpr int SIZE = 4; 

int main()
{
    whitelist _whitelist;
    // raw pointer to access student data
    const student *student_ptr;

    //  initialize data
    initailize(&_whitelist);

    //  student names
    string name[SIZE] = {"huzaifa", "umair", "mueeb",
                       //   for error checking
                       "unkown-name"};

    for (int i = 0; i < SIZE; i++)
    {
        // run time assertion check
        assert(name[i].length() < 15);
        // compile time assertion check
        static_assert(SIZE == 4, "array size is grater than 16");
        //  get pointer of ith student entry
        student_ptr = _whitelist.get_student_data(name[i]);
        //  if its not unkown name then enter this block
        if (student_ptr != nullptr)
            // just formating
            printf("%-5s : %-15s %-5s : %-10s %-5s : %-5d %-5s : %-5.2f\n",
                   "name", name[i].c_str(),
                   "Roll#", student_ptr->getRollNo().c_str(),
                   "Age", student_ptr->getAge(),
                   "CGPA", student_ptr->getCGPA());
        else
            printf("Function : %s === ERROR!: \"%s\" not found\n", __FUNCTION__, name[i].c_str());
    }
    return 0;
}

inline void initailize(whitelist *_whitelist)
{
    student student1("1", 20 + 1, 1.5 + .1),
        student2("2", 20 + 2, 1.5 + .2),
        student3("3", 20 + 3, 1.5 + .3);

    _whitelist->add_to_whitelist("huzaifa", student1);
    _whitelist->add_to_whitelist("umair", student2);
    _whitelist->add_to_whitelist("mueeb", student3);
}