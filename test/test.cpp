#include "student.h"
#include "whitelist.h"
#include "gtest/gtest.h"

using namespace std;

// #ifndef _StudentTest_H
// #define _StudentTest_H
class WhiteListTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    student1.initialzie("123", 26, 3.44);
    student2.initialzie("345", 27, 3.55);
  }

  student student1;
  student student2;
  whitelist _whitlist;
};

// #endif



TEST_F(WhiteListTest, DataEntryTestForStudentClass)
{
  EXPECT_EQ(student1.getRollNo(), "123") << "student roll no. != 123";
  EXPECT_EQ(student1.getAge(), 26) << "student age != 26";
  EXPECT_FLOAT_EQ(student1.getCGPA(), 3.44) << "student cgpa != 3.44";
}

TEST_F(WhiteListTest, DataEntryTestForWhiteList)
{
  _whitlist.add_to_whitelist("huzaifa", student1);

  EXPECT_TRUE(_whitlist.is_student_present("huzaifa")) << "huzaifa not found in whitlist";
  EXPECT_FALSE(_whitlist.is_student_present("unkown-name")) << "unknown name found in whitlist";

  EXPECT_EQ(_whitlist.get_student_data("huzaifa")->getRollNo(), "123") << "student roll no. != 123 in whitlist";
  EXPECT_EQ(_whitlist.get_student_data("huzaifa")->getAge(), 26) << "student age != 26 in whitlist";
  EXPECT_FLOAT_EQ(_whitlist.get_student_data("huzaifa")->getCGPA(), 3.44) << "student cgpa != 3.44 in whitlist";
  EXPECT_EQ(_whitlist.get_student_data("unknown-name"), nullptr) << "unkown name found in whitlist";
}

TEST_F(WhiteListTest, DataOverWritingTest)
{
  _whitlist.add_to_whitelist("huzaifa", student1);
  _whitlist.add_to_whitelist("huzaifa", student2);

  EXPECT_EQ(_whitlist.get_student_data("huzaifa")->getRollNo(), "345") << "student roll no. is not overwritten in whitlist";
  EXPECT_EQ(_whitlist.get_student_data("huzaifa")->getAge(), 27) << "student age is not overwritten in whitlist";
  EXPECT_FLOAT_EQ(_whitlist.get_student_data("huzaifa")->getCGPA(), 3.55) << "student cgpa is not overwritten in whitlist";
}

TEST_F(WhiteListTest, FailureTest)
{
  _whitlist.add_to_whitelist("huzaifa", student1);

  EXPECT_TRUE(_whitlist.is_student_present("unknown-name")) << "unknown name not found in whitlist";
}

int main()
{
  testing::InitGoogleTest();
  return RUN_ALL_TESTS();
}
