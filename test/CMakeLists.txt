# Executable name is same a project name
add_executable(student_test test.cpp)

# Just to differentiate between release and debug builds
set_target_properties(student_test PROPERTIES DEBUG_POSTFIX "_debug")

# To include header file that contains class definition
target_include_directories(student_test PUBLIC "${PROJECT_SOURCE_DIR}/include")

# To link libs
target_link_libraries(student_test PUBLIC student_entry_lib gtest_main)