#ifndef _STUDENT_H
#define _STUDENT_H

#include <string>
#include <map>
#include <vector>
#include <list>

class student
{
private:
  struct student_record
  {
    std::string roll_no;
    int age;
    float cgpa;
  } record;
  std::map<std::string /*subject name*/, int /*marks*/> result;

public:
  student(const std::string &roll_no, const int &age, const float &cgpa);
  student();
  void initialzie(const std::string &roll_no, const int &age, const float &cgpa);
  const std::string getRollNo() const;
  void setRollNo(const std::string &roll_no);
  int getAge() const;
  void setAge(const int &age);
  const float getCGPA() const;
  void setCGPA(const float &cgpa);
};

#endif