#ifndef _WHITELIST_H
#define _WHITELIST_H

#include "student.h"
#include <string>
#include <map>
#include <list>

class whitelist
{
private:
  std::map<std::string /* student name */, student * /* student entry pointer */> student_record;
  std::list<student /* student entry */> student_data_list;

public:
  void add_to_whitelist(const std::string &student_name, const student &_student);
  const bool is_student_present(const std::string &student_name) const;
  const student *get_student_data(const std::string &student_name) const;
};

#endif